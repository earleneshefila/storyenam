from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .models import Post
from .forms import PostForms
from .views import landing

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class UnitTest(TestCase):

    def test_landing(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing.html')
        self.assertContains(response,'Halo, apa kabar?',html = True)
    
    def create_models(self):
        new = Post.objects.create(status='This is test only')
        return new

    def test_models(self):
        s = self.create_models()
        self.assertTrue(isinstance(s, Post))
        self.assertTrue(s.__str__(), s.status)
        counting_all_status = Post.objects.all().count()
        self.assertEqual(counting_all_status, 1)

    def test_form(self):
        form_data = {
        'status' : 'Status for test only',
        }
        form = PostForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/', data = form_data)
        self.assertEqual(request.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        super(FunctionalTest, self).setUp()
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get('http://127.0.0.1:8000')

        self.assertInHTML('Halo, apa kabar?', self.browser.page_source)

        status = self.browser.find_element_by_id('id_status')
        submit = self.browser.find_element_by_id('submit')

        status.send_keys("Only for Testing")
        submit.send_keys(Keys.RETURN)

        self.assertInHTML("Only for Testing", self.browser.page_source)

    
