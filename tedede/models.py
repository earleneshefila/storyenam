from django.db import models

class Post(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.status