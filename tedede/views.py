from django.shortcuts import render,redirect 
from .models import Post
from .forms import PostForms

def landing(request):
    if request.method == "POST":
        form = PostForms(request.POST)
        if form.is_valid():
            status = form.save(commit=False)
            status.save()
            return redirect('landing')
    else:
        form = PostForms()
    status = Post.objects.all()
    return render (request, 'landing.html', {'form' : form, 'status' : status})
